#
#
# colorRec.py
#
# Demonstrates color based recognition using 
# normal (Gaussian) models of the colors
#
# Seth McNeill
# 2018 February 07

import sys
import numpy as np
import cv2
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab


# prints how to use the script
def usage():
  print "Usage: python colorRec.py train1Img train2Img testImg"

def bgr2rgb(img):
  return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

# executes if run as a script (not imported)
if __name__ == '__main__':
  if len(sys.argv) < 4:
    usage()
    sys.exit(1)
  train1ImgName = sys.argv[1]
  train2ImgName = sys.argv[2]
  testImgName = sys.argv[3]
  print "training on " + train1ImgName + " and " + train2ImgName
  print "testing " + testImgName

  img1Train = cv2.imread(train1ImgName)
  img2Train = cv2.imread(train2ImgName)
  imgTest = cv2.imread(testImgName)

  img1TrainHSV = cv2.cvtColor(img1Train, cv2.COLOR_BGR2HSV)
  img2TrainHSV = cv2.cvtColor(img2Train, cv2.COLOR_BGR2HSV)
  imgTestHSV = cv2.cvtColor(imgTest, cv2.COLOR_BGR2HSV)

  # build a model based on the red channels of img1 and img2
  img1TrRed = img1Train[:,:,2] # order is BGR -> 012
  img2TrRed = img2Train[:,:,2]

# extract red pixels as a single array
  img1TrRedPx = img1TrRed.reshape(img1TrRed.size)
  img2TrRedPx = img2TrRed.reshape(img2TrRed.size)
  c1Prior = 0.5
  c2Prior = 1 - c1Prior
  c1Mean = img1TrRedPx.mean()
  c1Std = img1TrRedPx.std()
  c2Mean = img2TrRedPx.mean()
  c2Std = img2TrRedPx.std()


  fig, axs1 = plt.subplots(2,1)
  n1, bins1, patches1 = axs1[0].hist(img1TrRedPx, 50, normed=True, label='Class 1', color='r')
  n2, bins2, patches2 =axs1[0].hist(img2TrRedPx, 50, normed=True, label='Class 2', color='b')

  step = 0.1
  x = np.arange(0,255+step, step)
  y1 = mlab.normpdf(x, c1Mean, c1Std)
  y2 = mlab.normpdf(x, c2Mean, c2Std)

  g1 = np.log(1/np.sqrt(2*np.pi*c1Std)) - 0.5*np.power((x - c1Mean)/c1Std,2) + np.log(c1Prior)
  g2 = np.log(1/np.sqrt(2*np.pi*c2Std)) - 0.5*np.power((x - c2Mean)/c2Std,2) + np.log(c2Prior)

  decisionPDF = np.argwhere(np.diff(np.sign(y1 - y2))).reshape(-1)
  decisionG = np.argwhere(np.diff(np.sign(g1 - g2))).reshape(-1)

# plot the histograms and pdfs
  axs1[0].plot(x, y1, label='NormPDF 1', color='r')
  axs1[0].plot(x, y2, label='NormPDF 2', color='b')
  axs1[0].set_title('Histograms of Pixel Values for Each Class', size=22)
  axs1[0].set_xlabel('Pixel Value', size=18)
  axs1[0].set_ylabel('Normalized Number of Pixels', size=18)
  axs1[0].legend(loc='best')

  axs1[1].plot(x,g1, 'r')
  axs1[1].plot(x,g2, 'b')
  axs1[1].fill_between(x, g1, 0, g1>g2, color='r')
  axs1[1].fill_between(x, g2, 0, g1<g2, color='b')
  axs1[1].set_title('Discriminant Functions - Boundary: %s' % x[decisionG], size=22)
  axs1[1].set_xlabel('Percentage of Pixels', size=18)
  axs1[1].set_ylabel('Magnitude', size=18)

  fig, axs = plt.subplots(2, 1)
  axs[0].imshow(bgr2rgb(img1Train), cmap='gray')
  axs[0].set_title('Class 1 Training Image', size=20)
  axs[1].imshow(bgr2rgb(img2Train), cmap='gray')
  axs[1].set_title('Class 2 Training Image', size=20)

# find the mask for the test image
  testRed = imgTest[:,:,2]
  testMask = testRed > x[decisionG]
#  testMask = np.zeros((testRed.shape[0], testRed.shape[1], 3))
#  testMask[testRed > decisionG] = [1,1,1]
#  testMask[testRed <= decisionG] = [0,0,0]

  fig, ax2 = plt.subplots(2,1)
  ax2[0].imshow(bgr2rgb(imgTest), cmap='gray', interpolation='bicubic')
#  ax2.xticks([]), plt.yticks([]) # hide tick values on axes
  ax2[0].set_title('Test Image', size=22)
  ax2[1].imshow(testMask, 'gray')

  plt.show()

  # see https://docs.opencv.org/3.1.0/d3/df2/tutorial_py_basic_ops.html
  # and https://henrydangprg.com/2016/06/26/color-detection-in-python-with-opencv/

